/**
 * Created by longsiwei on 18/4/21.
 */
import {loginRequest} from '../assets/util/request';
import Const from '../assets/util/const';

export const loginApi = {
  doLogin(values){
    return loginRequest(Const.SERVER_PREFIX_URL + '/login', {
      method: 'POST',
      body: JSON.stringify(values)
    }).then(res => {
      console.log("login api: " + res.userData.username);
      return res.userData;
    });
  }
};
