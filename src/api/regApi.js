/**
 * Created by longsiwei on 18/5/6.
 */
import {requestWithoutToken} from '../assets/util/request';
import Const from '../assets/util/const';

export const regApi = {
  doReg(values){
    return requestWithoutToken(Const.SERVER_PREFIX_URL + '/user/save', {
      method: 'POST',
      body: JSON.stringify(values)
    }).then(res => {
      return res;
    });
  }
};
