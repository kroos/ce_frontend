/**
 * Created by longsiwei on 18/4/21.
 */

export const sessionStorageApi = {
  saveUserInfoWithToken(userData, token){
    sessionStorage.setItem("userInfo", {...userData, token});
  },
  getLoginUserInfo(){
    let userStr = sessionStorage.getItem("userInfo");
    return JSON.parse(userStr);
  }
};
