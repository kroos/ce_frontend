export default [
  {
    path: '/',
    component: require('./assets/vue/pages/welcome/welcome.vue')
  },
  {
    path: '/reg/',
    component: require('./assets/vue/pages/reg/reg.vue'),
    options: {
      animate: true,
      reloadCurrent: true
    }
  },
  {
    path: '/reg2/:options',
    component: require('./assets/vue/pages/reg/reg_second.vue'),
    options: {
      animate: true,
      reloadCurrent: true
    }
  },
  {
    path: '/login/',
    component: require('./assets/vue/pages/login/login.vue')
  },
  {
    path: '/home/',
    component: require('./assets/vue/pages/home/home.vue')
  },
  {
    path: '/about/',
    component: require('./assets/vue/pages/about.vue')
  },
  {
    path: '/form/',
    component: require('./assets/vue/pages/form.vue')
  },
  {
    path: '/dynamic-route/blog/:blogId/post/:postId/',
    component: require('./assets/vue/pages/dynamic-route.vue')
  },
  {
    path: '/panel-left/',
    component: require('./assets/vue/pages/panel-left.vue')
  },
  {
    path: '/color-themes/',
    component: require('./assets/vue/pages/color-themes.vue')
  },
  {
    path: '/chat/',
    component: require('./assets/vue/pages/chat.vue')
  },
  {
    path: '/vuex/',
    component: require('./assets/vue/pages/vuex.vue')
  },
]
