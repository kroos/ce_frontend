/**
 * Created by longsiwei on 18/4/20.
 */
import fetch from 'isomorphic-fetch';

import {sessionStorageApi} from '../../api/sessionStorageApi';

function checkStatus(response) {
  console.log("checkStatus: " + response.status);
  if (response.status >= 200 && response.status < 300) {
    return response;
  }else if(response.status == 403){
    location.href = "/login";
  }

  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

export async function loginRequest(url, options){
  const response = await fetch(url, options);
  console.log("login request response: " + JSON.stringify(response));
  checkStatus(response);
  console.log("req response: " + response.headers.get('authorization'));
  let token = response.headers.get('authorization');
  let userData = await response.json();
  console.log("req userData: " + userData.username);
  //token = token.replace("Bearer ", "");

  sessionStorageApi.saveUserInfoWithToken(userData, token);
  return {token, userData};
}

export async function requestWithoutToken(url, options){

  let response;

  let ret = {
    data: {},
    headers: {},
  };

  try {
    response = await fetch(url, options);

    checkStatus(response);

    const data = await response.json();

    ret = {
      data,
      headers: {},
    };

    if (response.headers.get('x-total-count')) {
      ret.headers['x-total-count'] = response.headers.get('x-total-count');
    }
  }catch(e){
    console.error("req fetch error: ", e);
  }

  return ret;
}

export async function request(url, options = {}) {
  let userInfo = sessionStorageApi.getLoginUserInfo();
  options.headers = {"authorization": userInfo.token};

  let response;

  let ret = {
    data: {},
    headers: {},
  };

  try {
    response = await fetch(url, options);

    checkStatus(response);

    const data = await response.json();

    ret = {
      data,
      headers: {},
    };

    if (response.headers.get('x-total-count')) {
      ret.headers['x-total-count'] = response.headers.get('x-total-count');
    }
  }catch(e){
    console.error("req fetch error: ", e);
  }

  return ret;
}
