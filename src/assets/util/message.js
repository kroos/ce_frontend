/**
 * Created by longsiwei on 18/4/20.
 */
export default {
  REGISTER_GENDER_NOT_SELECT: 'Please select your gender.',
  REGISTER_FAIL: 'Register as a User failed, Please Contact Administrator.'
};
