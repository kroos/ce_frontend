/**
 * Created by longsiwei on 18/6/7.
 */
export function navigateToHistory(router, url){
  router.navigate(url, {
    reloadCurrent: true,
    ignoreCache: true,
  });
}
