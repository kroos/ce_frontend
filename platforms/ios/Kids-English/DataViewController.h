//
//  DataViewController.h
//  Kids-English
//
//  Created by 龙思纬 on 18/4/23.
//
//

#import <UIKit/UIKit.h>

@interface DataViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *dataLabel;
@property (strong, nonatomic) id dataObject;

@end

