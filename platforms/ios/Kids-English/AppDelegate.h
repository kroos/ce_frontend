//
//  AppDelegate.h
//  Kids-English
//
//  Created by 龙思纬 on 18/4/23.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

