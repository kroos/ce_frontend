//
//  RootViewController.h
//  Kids-English
//
//  Created by 龙思纬 on 18/4/23.
//
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController <UIPageViewControllerDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;

@end

